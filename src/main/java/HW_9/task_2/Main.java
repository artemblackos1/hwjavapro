package HW_9.task_2;

public class Main {
    public static void main(String[] args) {
        Box <Orange> orangeBox = new Box<>(7);
        Box <Apple> appleBox = new Box<>(8);

        Orange orange = new Orange();
        Apple apple = new Apple();

        System.out.println("The response about the success of the add operation is " + orangeBox.addFruit(orange));
        System.out.println("Box with orange weight " + orangeBox.getWeight());
        System.out.println("The response about the success of the add operation is " + orangeBox.addFruits(orange, orange, orange));
        System.out.println("Box orange weight " + orangeBox.getWeight());

        System.out.println();
        System.out.println("The response about the success of the add operation is " + appleBox.addFruit(apple));
        System.out.println("Box with apples weight " + appleBox.getWeight());
        System.out.println("The response about the success of the add operation is " + appleBox.addFruits(apple, apple));
        System.out.println("Box apple weight " + appleBox.getWeight());

        System.out.println();
        System.out.println("Answer from compare method = " + appleBox.compare(orangeBox));
        System.out.println("The response about the success of the add operation is " + appleBox.addFruits(apple, apple, apple));
        System.out.println("Answer from compare method = " + appleBox.compare(orangeBox));

        System.out.println();
        Box <Apple> appleBox2 = new Box<>(8);
        System.out.println("The response about the success of the add operation is " + appleBox2.addFruit(apple));

        System.out.println();
        System.out.println("appleBox weight before merge: " + appleBox.getWeight());
        System.out.println("appleBox2 weight before merge: " + appleBox2.getWeight());

        appleBox2.merge(appleBox);
        System.out.println("appleBox weight after merge: " + appleBox.getWeight());
        System.out.println("appleBox2 weight after merge: " + appleBox2.getWeight());





    }
}
