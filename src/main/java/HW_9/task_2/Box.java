package HW_9.task_2;

public class Box <T extends Fruit>{
    private final Object [] fruitsArray;
    private int currentPointer = 0;

    public Box(int size) {
        this.fruitsArray = new Object[size];
    }

    public boolean addFruit(T fruit){
        if(fruit == null){
            throw new IllegalArgumentException("The elements is null");
        }
        if(currentPointer < fruitsArray.length) {
            fruitsArray[currentPointer] = fruit;
            currentPointer++;
            return true;
        } else {
            System.out.println("Box is full. Adding fruit is not exist");
            return false;
        }


    }

    public boolean addFruits(T... fruits) {
        if (fruits == null) {
            throw new IllegalArgumentException("The passed array of elements is null");
        }
        boolean isCanPut = fruits.length > fruitsArray.length;
        boolean isCanPut2 = (fruitsArray.length - currentPointer) < fruits.length;

        if (!(isCanPut || isCanPut2)) {
            for (int i = 0; i < fruits.length; i++) {
                addFruit(fruits[i]);
            }
            return true;
        } else {
            System.out.println("Box is full. Adding fruit is not exist");
            return false;
        }
    }

    public float getWeight(){
        float allWeight = 0;
        for (int i = 0; i < currentPointer; i++) {
            Fruit fruit = (Fruit) fruitsArray[i];
            allWeight += fruit.getWeight();
        }
        return allWeight;

    }

    public boolean compare(Box<?> someBox){
      return (this.getWeight() - someBox.getWeight()) == 0;
    }

    public void merge(Box<T> someBox){
        if (this == someBox) throw new IllegalArgumentException("it's the same box");

        if (currentPointer + someBox.currentPointer > fruitsArray.length) {
            throw new IllegalArgumentException("Not enough space to merge boxes.");
        }

        if (this.getClass() != someBox.getClass()) {
            throw new IllegalArgumentException("Cannot merge boxes with different fruit types.");
        }

        Object[] tempFruits = new Object[fruitsArray.length];
        for (int i = 0; i < currentPointer; i++) {
            tempFruits[i] = fruitsArray[i];
        }

        for (int i = 0; i < someBox.currentPointer; i++) {
            T frut = (T) someBox.get(i);
            fruitsArray[currentPointer + i] = frut;
        }
        currentPointer += someBox.currentPointer;

        for (int i = 0; i < someBox.currentPointer; i++) {
            someBox.fruitsArray[i] = null;
        }
        someBox.currentPointer = 0;
    }

    private T get(int i) {
        return (T) fruitsArray[i];
    }



}
