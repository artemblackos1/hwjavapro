package HW_8.zeroTask;

import HW_6.zeroTask.GeometricFigure;

public class Circule implements GeometricFigure {
    private double radius;

    public Circule(double radius) {
        this.radius = radius;
    }

    @Override
    public double square() {
        double s = Math.PI * Math.pow(radius, 2);
        return s;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
