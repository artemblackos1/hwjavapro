package HW_8.zeroTask;

import HW_6.zeroTask.Circule;
import HW_6.zeroTask.GeometricFigure;
import HW_6.zeroTask.SquareFigure;
import HW_6.zeroTask.Triangle;

public class Main {
    public static void main(String[] args) {
        HW_6.zeroTask.GeometricFigure[] figures  = {new Circule(5.5), new SquareFigure(88), new Triangle(3,4,4.4)};

        System.out.println("Total square all figures: " + allSquare(figures));

    }

    private static double allSquare(GeometricFigure[] geometricFigure){
        double s = 0;
        for(int i = 0; i < 3; i++){
            s += geometricFigure[i].square();
        }
        return s;
    }
}
