package HW_8.exception;

public class ArraySizeException extends IllegalArgumentException{
    public ArraySizeException(String s) {
        super(s);
    }
}
