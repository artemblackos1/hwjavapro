package HW_8.exception;

public class ArrayDataException extends IllegalArgumentException{
    public ArrayDataException(String s) {
        super(s);
    }
}
