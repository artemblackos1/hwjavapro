package HW_8.enoughterStory;

public class Human implements Participant {
    private String name;
    private int maxJumpHigh;
    private int maxRunDistance;
    private boolean isDone;

    public Human(String name, int maxJumpHigh, int maxRunDistance) {
        this.name = name;
        this.maxJumpHigh = maxJumpHigh;
        this.maxRunDistance = maxRunDistance;
        this.isDone = true;
    }

    @Override
    public void jump(int high, String nameBarrier) {
        if(!isDone) return;

        if(high <= maxJumpHigh){
            System.out.println("Participant " + name + " passed the barrier " + nameBarrier + " on distance " + high + " m.");
        } else {
            System.out.println("Participant " + name + " not passed the barrier " + nameBarrier + " on distance " + high + " m. Passed only " + maxJumpHigh );
            isDone = false;
        }
    }

    @Override
    public void run(int distance, String nameBarrier) {
        if(!isDone) return;

        if(distance <= maxRunDistance){
            System.out.println("Participant " + name + " passed the barrier " + nameBarrier + " on distance " + distance + " m.");
        } else {
            System.out.println("Participant " + name + " not passed the barrier " + nameBarrier + " on distance " + distance + " m. Passed only " + maxRunDistance );
            isDone = false;
        }
    }
    @Override
    public boolean isDone() {
        return isDone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxJumpHigh() {
        return maxJumpHigh;
    }

    public void setMaxJumpHigh(int maxJumpHigh) {
        this.maxJumpHigh = maxJumpHigh;
    }

    public int getMaxRunDistance() {
        return maxRunDistance;
    }

    public void setMaxRunDistance(int maxRunDistance) {
        this.maxRunDistance = maxRunDistance;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
