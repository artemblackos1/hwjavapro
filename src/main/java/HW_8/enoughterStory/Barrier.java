package HW_8.enoughterStory;

public interface Barrier {
    void overcome(Participant participant);
}
