package HW_8.enoughterStory;

import HW_7.exception.ArrayDataException;
import HW_7.exception.ArraySizeException;

import java.util.Scanner;

public class Competition {
    private static String name;
    private static int maxJumpHigh;
    private static int maxRunDistance;
    private static int high;
    private static int distance;

    public static Participant[] createParticipantsForCompetition(String... participant) {

        if(participant.length < 1){
            throw new IllegalArgumentException("Array cannot have any Participant");
        } if(participant == null){
            throw new ArraySizeException("The passed array of Participant is null");
        } for(String n: participant){
            if(!n.equals("Robot") && !n.equals("Cat") && !n.equals("Human")) {
                throw new ArrayDataException("Wrong type of Participant, change data, " + n + " not equal Robot, Cat or Human. Try again");
            }
        }

        Participant[] participants = new Participant[participant.length];

        for (int i = 0; i < participant.length; i++) {
            if (participant[i].equals("Robot")) {
                System.out.println("Say me " + participant[i] + "'s name:");
                name = scanData();

                System.out.println("Say me " + participant[i] + "'s maxJumpHigh:");
                try {
                    maxJumpHigh = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Robot's maxJumpHigh");
                }

                System.out.println("Say me " + participant[i] + "'s maxRunDistance:");
                try {
                    maxRunDistance = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Robot's maxRunDistance");
                }

                participants[i] = new Robot(name, maxJumpHigh, maxRunDistance);
            }
            if (participant[i].equals("Cat")) {
                System.out.println("Say me " + participant[i] + "'s name:");
                name = scanData();

                System.out.println("Say me " + participant[i] + "'s maxJumpHigh:");
                try {
                    maxJumpHigh = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Cat's maxJumpHigh");
                }

                System.out.println("Say me " + participant[i] + "'s maxRunDistance:");
                try {
                    maxRunDistance = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Cat's maxRunDistance");
                }

                participants[i] = new Cat(name, maxJumpHigh, maxRunDistance);
            }
            if (participant[i].equals("Human")) {
                System.out.println("Say me " + participant[i] + "'s name:");
                name = scanData();

                System.out.println("Say me " + participant[i] + "'s maxJumpHigh:");
                try {
                    maxJumpHigh = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Human's maxJumpHigh");
                }

                System.out.println("Say me " + participant[i] + "'s maxRunDistance:");
                try {
                    maxRunDistance = Integer.parseInt(scanData());
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Human's maxRunDistance");
                }

                participants[i] = new Human(name, maxJumpHigh, maxRunDistance);
            }
        }
        return participants;
    }


    public static Barrier[] createBarrierForCompetition(String... barrier) {
        if(barrier.length < 1){
            throw new IllegalArgumentException("Array cannot have any Barrier");
        } if(barrier == null){
            throw new ArraySizeException("The passed array of Barrier is null");
        } for(String n: barrier){
            if(!n.equals("Wall") && !n.equals("RunningTrack")) {
                throw new ArrayDataException("Wrong type of Barrier, change data, " + n + " not equal Wall, RunningTrack. Try again");
            }
        }

        Barrier[] barriers = new Barrier[barrier.length];

        System.out.println("Lets create barriers");

        for (int i = 0; i < barrier.length; i++) {
            if (barrier[i].equals("Wall")) {
                System.out.println("Say me " + barrier[i] + "'s high:");

                try {
                    high = Integer.parseInt(scanData());
                }catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in Wall's high");
                }

                barriers[i] = new Wall(high);
            }
            if (barrier[i].equals("RunningTrack")) {
                System.out.println("Say me " + barrier[i] + "'s distance:");

                try {
                distance = Integer.parseInt(scanData());
                }catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in RunningTrack's distance");
                }

                barriers[i] = new RunningTrack(distance);
            }
        }
        return barriers;
    }



    public static void doCompetition(Participant[] participants, Barrier[] barriers){

        for(Participant participant: participants){
            for (Barrier barrier : barriers){
                barrier.overcome(participant);
                if(!participant.isDone()){
                    break;
                }
            }
        }
    }
        private static String scanData(){
            Scanner scanner = new Scanner(System.in);
            return scanner.nextLine();
        }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Competition.name = name;
    }

    public static int getMaxJumpHigh() {
        return maxJumpHigh;
    }

    public static void setMaxJumpHigh(int maxJumpHigh) {
        Competition.maxJumpHigh = maxJumpHigh;
    }

    public static int getMaxRunDistance() {
        return maxRunDistance;
    }

    public static void setMaxRunDistance(int maxRunDistance) {
        Competition.maxRunDistance = maxRunDistance;
    }

    public static int getHigh() {
        return high;
    }

    public static void setHigh(int high) {
        Competition.high = high;
    }

    public static int getDistance() {
        return distance;
    }

    public static void setDistance(int distance) {
        Competition.distance = distance;
    }
}
