package HW_2.aboutCar;

public class Car {
    public void start(){
        startElectricity();
        startCommand();
        startFuelSystem();
    }

    private void startElectricity(){
        System.out.println("Starting the electrical system");
    }

    private void startCommand(){
        System.out.println("Starting the command system");
    }

    private void startFuelSystem(){
        System.out.println("Starting the fuel system");
    }
}
