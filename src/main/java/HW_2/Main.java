package HW_2;

public class Main {
    public static void main(String[] args) {
        FullName fullName = new FullName("John", "Ramos", "Smith");

        Employee employee = new Employee(fullName, "Developer", "john@gmail.com", "+380971234567", 44);

        System.out.println("Full Name: " + employee.getFullName().getFirstName() + " " +
                employee.getFullName().getMiddleName() + " " + employee.getFullName().getLastName() +
                ", Position: " + employee.getPosition() + ", Email: " + employee.getEmail() +
                ", Phone: " + employee.getPhone() + ", Age: " + employee.getAge());
    }
}
