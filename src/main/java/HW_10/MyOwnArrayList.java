package HW_10;

import java.util.*;

public class MyOwnArrayList<E> implements List<E> {
    private static final int DEF_SIZE = 5;
    private E[] element;
    private int size;

    public MyOwnArrayList() {
        this.element = (E[]) new Object[DEF_SIZE];
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    @Override
    public E[] toArray() {
        return (E[]) Arrays.copyOf(element, size);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if(a.length < size){
            return (T[]) Arrays.copyOf(element, size, a.getClass());
        }
        System.arraycopy(element, 0, a, 0, size);
        if(a.length > size){
            a[size] = null;
        }
        return a;
    }

    @Override
    public boolean add(E e) {
        ensureCapacity(size+1);
        element[size] = e;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object obj) {
        int el = indexOf(obj);
        if(el >= 0){
            remove(el);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object el: c) {
            if(contains(c)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int startSize = size;
        for (E el: c ) {
            add(el);
        }
        return startSize > size;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        int startSize = size;
        for (E el: c) {
            add(index, el);
        }
        return startSize > size;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean success = false;
        for (Object el: c) {
            if(remove(el)){
                success = true;
            }
        }
        return success;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean success = false;
        for (int i = size - 1; i >= 0 ; i--) {
            if(!contains(element[i])){
                remove(i);
                success = true;
            }
        }
        return success;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            element[i] = null;
        }
        size = 0;
    }

    @Override
    public E get(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        return (E) element[index];
    }

    @Override
    public E set(int index, E elementt) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        E currentValue = (E) element[index];
        element[index] = elementt;
        return currentValue;
    }

    @Override
    public void add(int index, E elementt) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        ensureCapacity(index + 1);
        System.arraycopy(element, index, element,index + 1, size - index);
        element[index] = elementt;
        size++;

    }

    @Override
    public E remove(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        E currentValue = (E) element[index];
        int checkPlace = size - index - 1;
        if(checkPlace > 0){
            System.arraycopy(element, index + 1, element, index, checkPlace);
        }
        element[--size] = null;
        return currentValue;
    }

    @Override
    public int indexOf(Object obj) {
        if(obj == null){
            throw new IllegalArgumentException("You passed nothing to compare, pass an element");
        } else {
            for (int i = 0; i < size; i++) {
                if(obj.equals(element[i])){
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object obj) {
        if(obj == null){
            throw new IllegalArgumentException("You passed nothing to compare, pass an element");
        } else {
            for (int i = size - 1; i >= 0; i--) {
                if(obj.equals(element[i])){
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
   public ListIterator<E> listIterator() {
       return new MyListIterator(0);
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException("You give me Index " + index + " I have size " + size);
        }
        return new MyListIterator(index);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        if(fromIndex < 0 || toIndex > size || fromIndex > toIndex){
            throw new IndexOutOfBoundsException("Check your parameters fromIndex and toIndex");
        }
        MyOwnArrayList<E> subList = new MyOwnArrayList<>();

        for (int i = fromIndex; i < toIndex; i++) {
            subList.add(element[i]);
        }

        return subList;
    }

    private void ensureCapacity(int minCapacity) {
        if (element.length < minCapacity) {
            int newCapacity = Math.max(minCapacity, element.length * 2);
            element = Arrays.copyOf(element, newCapacity);
        }
    }

     private class MyIterator implements Iterator<E>{
        private int actualStep = 0;

         @Override
        public boolean hasNext() {
            return actualStep < size;
        }

        @Override
        public E next() {
            if(!hasNext()){
                throw new NoSuchElementException();
            }
            return (E) element[actualStep++];
        }
    }
    private class MyListIterator implements ListIterator<E>{
        private int cIndex;

        public MyListIterator(int cIndex) {
            if (cIndex < 0 || cIndex > size) {
                throw new IndexOutOfBoundsException("Index: " + cIndex + ", Size: " + size);
            }
            this.cIndex = cIndex;
        }

        @Override
        public boolean hasNext() {
            return cIndex < size;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return get(cIndex++);
        }

        @Override
        public boolean hasPrevious() {
            return cIndex > 0;
        }

        @Override
        public E previous() {
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            return get(--cIndex);
        }

        @Override
        public int nextIndex() {
            return cIndex;
        }

        @Override
        public int previousIndex() {
            return cIndex - 1;
        }

        @Override
        public void remove() {
            if (cIndex < 0 || cIndex >= size) {
                throw new IllegalStateException();
            }
            MyOwnArrayList.this.remove(cIndex);
        }

        @Override
        public void set(E e) {
            if (cIndex < 0 || cIndex >= size) {
                throw new IllegalStateException();
            }
            MyOwnArrayList.this.set(cIndex, e);
        }

        @Override
        public void add(E e) {
            MyOwnArrayList.this.add(cIndex++, e);
        }
    }
}

