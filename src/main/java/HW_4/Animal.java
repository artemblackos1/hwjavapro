package HW_4;

public abstract class Animal {
    private static int countAnimals;
    private String name;

    public Animal(String name) {
        this.name = name;
        countAnimals++;
    }

    public abstract void run(int len);

    public abstract void swim(int len);

    public static int getCountAnimals() {
        return countAnimals;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
