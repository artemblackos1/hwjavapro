package HW_4;

public class Cat extends Animal{
    private static final int MAX_RUN_DISTANCE = 200;
    private static final int MAX_SWIM_DISTANCE = 0;

    public Cat(String name) {
        super(name);
    }

    @Override
    public void run(int len){
        if(len <= MAX_RUN_DISTANCE) {
            System.out.println("Cat " + super.getName() + " runs " + len + " m");
        } else {
            System.out.println("The cat can't run this distance");
        }
    }

    @Override
    public void swim(int len){
        if(len <= MAX_SWIM_DISTANCE) {
            System.out.println("Cat " + super.getName() + " swims " + len + " m");
        } else {
            System.out.println("The cat can't swim this distance");
        }
    }
}
