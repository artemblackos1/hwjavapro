package HW_4;

public class Dog extends Animal{
    private static final int MAX_RUN_DISTANCE = 500;
    private static final int MAX_SWIM_DISTANCE = 10;

    public Dog(String name) {
        super(name);
    }

    @Override
    public void run(int len){
        if (len <= MAX_RUN_DISTANCE) {
            System.out.println("Dog " + super.getName() + " runs " + len + " m");
        } else {
            System.out.println("The dog can't run this distance");
        }
    }

    @Override
    public void swim(int len){
        if (len <= MAX_SWIM_DISTANCE) {
            System.out.println("Dog " + super.getName() + " swims " + len + " m");
        } else {
            System.out.println("The dog can't swim this distance");
        }
    }


}
