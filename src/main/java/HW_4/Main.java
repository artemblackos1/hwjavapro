package HW_4;

public class Main {
    public static void main(String[] args) {
        Dog dogBobik = new Dog("Bobik");
        Cat catPersik = new Cat("Persik");
        Cat catBonya = new Cat("Bonya");

        dogBobik.run(300);
        dogBobik.swim(5);

        catPersik.run(100);
        catPersik.swim(50);

        catBonya.run(40);

        System.out.println();
        System.out.println("Count create of Animals = " + Animal.getCountAnimals());
    }
}
