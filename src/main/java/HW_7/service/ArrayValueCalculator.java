package HW_7.service;

import HW_7.exception.ArrayDataException;
import HW_7.exception.ArraySizeException;

public class ArrayValueCalculator {

    public static int doCalc(String [][] array, int size){
        checkArraySize(array,size);
        return parseIntAndSum(array);

    }

    private static void checkArraySize(String[][] array, int sizeArr){
        if(sizeArr < 1){
            throw new IllegalArgumentException("Array cannot have 0 elements");
        } if(array == null){
            throw new ArraySizeException("The passed array of elements is null");
        } if(array.length != sizeArr){
            throw new ArraySizeException("Invalid array size, we need " + sizeArr + " but we have " + array.length);
        } for(int i = 0; i < array.length; i++){
                if(array[i].length != sizeArr){
                    throw  new ArraySizeException("Invalid array size, do not match declared " + sizeArr + "x" + sizeArr);
                }
            }
    }

    private static int parseIntAndSum(String[][] array){
        int sum = 0;
        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                try {
                    sum += Integer.parseInt(array[i][j]);
                } catch (NumberFormatException e){
                    throw new ArrayDataException("Wrong data type in cell i = " + (i+1) + " j = " + (j+1));
                }
            }
        }
        return sum;
    }
}
