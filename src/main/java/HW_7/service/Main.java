package HW_7.service;

import static java.lang.Math.random;

public class Main {
    public static void main(String[] args) {
        String array[][] = fillArray(4,4);

        System.out.println(ArrayValueCalculator.doCalc(array,4));

    }

    public static String[][] fillArray(int x, int y){
        String arr[][] = new String[x][y];
        for (int i = 0; i < x; i++){
            for (int j = 0; j < y; j++) {
                arr[i][j] = String.valueOf((int) (Math.random() * 100));
            }
        }
        return arr;
    }
}
