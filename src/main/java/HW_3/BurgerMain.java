package HW_3;

public class BurgerMain {
    public static void main(String[] args) {
        Burger standartBurger = new Burger("bun with sesame", "beef", "cheddar","green","mayo");
        Burger dietaryBurger = new Burger("bun with sesame", "beef", "cheddar","green");
        Burger doableMeatBurger = new Burger("bun with sesame", "pork", "cheddar","green","mayo",true);
    }
}
