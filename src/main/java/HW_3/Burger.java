package HW_3;

public class Burger {
    private String bun;
    private String meat;
    private String chess;
    private String green;
    private String mayo;

    public Burger(String bun, String meat, String chess, String green, String mayo) {
        this.bun = bun;
        this.meat = meat;
        this.chess = chess;
        this.green = green;
        this.mayo = mayo;
        System.out.println("burger composition: " + getBun() + ", " + getMeat() + ", " + getChess() + ", " + getGreen() + ", " + getMayo());

    }

    public Burger(String bun, String meat, String chess, String green) {
        this.bun = bun;
        this.meat = meat;
        this.chess = chess;
        this.green = green;
        System.out.println("burger composition: " + getBun() + ", " + getMeat() + ", " + getChess() + ", " + getGreen());
    }

    public Burger(String bun, String meat, String chess, String green, String mayo, boolean doubleMeat) {
        this.bun = bun;
        this.meat = doubleMeat ? (meat + " x2") : meat;
        this.chess = chess;
        this.green = green;
        this.mayo = mayo;
        System.out.println("burger composition: "  + getBun() + ", " + getMeat() + ", " + getChess() + ", " + getGreen() + ", " + getMayo());
    }

    public String getBun() {
        return bun;
    }

    public void setBun(String bun) {
        this.bun = bun;
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getChess() {
        return chess;
    }

    public void setChess(String chess) {
        this.chess = chess;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getMayo() {
        return mayo;
    }

    public void setMayo(String mayo) {
        this.mayo = mayo;
    }


}
