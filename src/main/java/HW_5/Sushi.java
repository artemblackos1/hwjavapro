package HW_5;

public class Sushi {
    private final RiceType rice;
    private final FishType fish;
    private final VegetableType vegetables;
    private final SauceType sauce;

    private Sushi(RiceType rice, FishType fish, VegetableType vegetables, SauceType sauce) {
        this.rice = rice;
        this.fish = fish;
        this.vegetables = vegetables;
        this.sauce = sauce;
    }

    public static SushiBuilder builder(){
        return new SushiBuilder();
    }

    public RiceType getRice() {
        return rice;
    }

    public FishType getFish() {
        return fish;
    }

    public VegetableType getVegetables() {
        return vegetables;
    }

    public SauceType getSauce() {
        return sauce;
    }

    @Override
    public String toString() {
        return "Sushi{" +
                "rice=" + rice +
                ", fish=" + fish +
                ", vegetables=" + vegetables +
                ", sauce=" + sauce +
                '}';
    }



    public static class SushiBuilder{

        private RiceType rice;
        private FishType fish;
        private VegetableType vegetables;
        private SauceType sauce;

        public Sushi build(){
            return new Sushi(
                    this.rice,
                    this.fish,
                    this.vegetables,
                    this.sauce);
        }

        private SushiBuilder() {

        }

        public SushiBuilder rice(RiceType rice) {
            this.rice = rice;
            return this;
        }

        public SushiBuilder fish(FishType fish) {
            this.fish = fish;
            return this;
        }

        public SushiBuilder vegetables(VegetableType vegetables) {
            this.vegetables = vegetables;
            return this;
        }

        public SushiBuilder sauce(SauceType sauce) {
            this.sauce = sauce;
            return this;
        }
    }


    public enum RiceType{
        BRAUN, WHITE
    }
    public enum FishType{
        SALMON, EEL, STURGEON
    }
    public enum VegetableType{
        CUCUMBER, TOFU, AVOCADO
    }
    public enum SauceType{
        MAYO, CHEESY
    }
}


