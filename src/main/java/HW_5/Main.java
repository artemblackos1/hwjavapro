package HW_5;

import HW_5.Sushi.SushiBuilder;

public class Main {
    public static void main(String[] args) {
        SushiBuilder sushiBuilderGeneral = Sushi.builder()
                .rice(Sushi.RiceType.WHITE)
                .fish(Sushi.FishType.SALMON)
                .vegetables(Sushi.VegetableType.CUCUMBER);

        sushiBuilderGeneral
                .sauce(Sushi.SauceType.CHEESY);

        Sushi sushiGeneral = sushiBuilderGeneral.build();
        System.out.println(sushiGeneral);


        System.out.println();


        SushiBuilder sushiBuilderDiet = Sushi.builder()
                .rice(Sushi.RiceType.BRAUN)
                .fish(Sushi.FishType.EEL)
                .vegetables(Sushi.VegetableType.AVOCADO);

        Sushi sushiDiet = sushiBuilderDiet.build();
        System.out.println(sushiDiet);

    }
}
