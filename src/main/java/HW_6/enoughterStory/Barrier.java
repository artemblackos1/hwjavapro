package HW_6.enoughterStory;

public interface Barrier {
    void overcome(Participant participant);
}
