package HW_6.enoughterStory;

public class Main {
    public static void main(String[] args) {
        Participant [] participants = {
                new Human("John", 2, 1000),
                new Cat("Tom", 3, 200),
                new Robot("Chappy", 1, 100)};

        Barrier [] barriers = {new RunningTrack(150), new Wall(2), new RunningTrack(1000), new Wall(4)};

        for(Participant participant: participants){
            for (Barrier barrier : barriers){
                barrier.overcome(participant);
                if(!participant.isDone()){
                    break;
                }
            }
        }

    }
}
