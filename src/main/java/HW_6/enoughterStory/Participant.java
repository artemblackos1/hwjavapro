package HW_6.enoughterStory;

public interface Participant {
    void jump(int high, String name);
    void run(int distance, String name);
    boolean isDone();
}
