package HW_6.enoughterStory;

public class Wall implements Barrier{
    final String name = "wall";
    int high;

    public Wall(int high) {
        this.high = high;
    }

    @Override
    public void overcome(Participant participant) {
        participant.jump(high, name);
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }
}
