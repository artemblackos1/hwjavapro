package HW_6.enoughterStory;

public class RunningTrack implements Barrier{
    final String name  = "runningTrack";
    int distance;

    public RunningTrack(int distance) {
        this.distance = distance;
    }

    @Override
    public void overcome(Participant participant) {
        participant.run(distance, name);
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
