package HW_6.zeroTask;

public class SquareFigure implements GeometricFigure{
    private double side;

    public SquareFigure(double side) {
        this.side = side;
    }

    @Override
    public double square() {
        double s = side * side;
        return s;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
