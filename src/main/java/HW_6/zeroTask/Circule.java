package HW_6.zeroTask;

public class Circule implements GeometricFigure {
    private double radius;

    public Circule(double radius) {
        this.radius = radius;
    }

    @Override
    public double square() {
        double s = Math.PI * radius * radius;
        return s;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
