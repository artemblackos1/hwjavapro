Рефакторинг кода HW_6 в рамках ДЗ 8:
1. Заменил radius*radius на использование Math.pow(radius, 2);
2. С класса Main удалена вся бизнес-логика и перенесена в класс Competition
3. В классе Competition:
а) использован варарг в методах createParticipantsForCompetition и createBarrierForCompetition
б) созданы отдельные методы для создания обьектов Participant и Barrier вместо харткода в методе Main
в) добавлен метод scanData для получения информации с консоли от пользователя
г) перенесён метод doCompetition
д) добавлены Exceptions - IllegalArgumentException, ArraySizeException, ArrayDataException